﻿using WebApi.Models;

namespace WebApi.Repositories
{
    public interface IOrderRepository
    {
        public void AddOrder(Order order);
        public List<Order> GetAllOrders();
        public Order GetOrder(int id);
        public void DeleteOrder(int id);
    }
}
