﻿namespace WebApi.Enums
{
    public enum DeliveryType
    {
        Delivery,
        SelfPickup
    }
}