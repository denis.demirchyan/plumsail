﻿using Microsoft.EntityFrameworkCore;
using WebApi.Repositories;

namespace WebApi.Configuration
{
    public static class DatabaseConfiguration
    {
        public static void ConfigureDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<OrdersContext>(options => options.UseSqlServer(configuration.GetConnectionString("OrdersDatabase")));
        }
    }
}
