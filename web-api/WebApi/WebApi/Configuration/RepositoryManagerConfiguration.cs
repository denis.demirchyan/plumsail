﻿using WebApi.Repositories;

namespace WebApi.Configuration
{
    public static class RepositoryManagerConfiguration
    {
        public static void AddRepositoryManager(this IServiceCollection services)
        {
            services.AddScoped<IOrderRepository, OrderRepository>();
        }
    }
}
