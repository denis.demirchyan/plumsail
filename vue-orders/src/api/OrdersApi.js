import axios from 'axios'
import { RequestResult } from './models/RequestResult'

export class OrdersApi {
    _httpClient;

    constructor (url) {
      this._httpClient = axios.create({
        baseURL: url,
        timeout: 15000
      })
    }

    async addOrder (order) {
      const result = await this.request(async () => {
        return await this._httpClient.post('/AddOrder', order)
      })
      return result
    }

    async getOrders () {
      const result = await this.request(async () => {
        return await this._httpClient.get('/GetAllOrders')
      })
      return result
    }

    async deleteOrder (id) {
      const result = await this.request(async () => {
        return await this._httpClient.delete('/DeleteOrder', { params: { id: id } })
      })
      return result
    }

    async request (method) {
      const result = new RequestResult()
      try {
        const response = await method()
        result.StatusCode = response.status
        if (response.status === 200) {
          result.Data = response.data
        } else {
          result.IsSuccess = false
          result.Errors.push(response?.statusText)
        }
      } catch (error) {
        result.IsSuccess = false
        result.Errors.push(error.message)
      }
      return result
    }
}
