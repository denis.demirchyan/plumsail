﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;
        public OrdersController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        [HttpPost(Name = "AddOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public ActionResult<int> AddOrder([FromBody] Order order)
        {
            if (_orderRepository.GetOrder(order.Id) != null)
            {
                return Conflict($"Order with id {order.Id} already exist.");
            }
            _orderRepository.AddOrder(order);
            return Ok(order.Id);
        }

        [HttpGet(Name = "GetAllOrders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<List<Order>> GetAllOrders()
        {
            return Ok(_orderRepository.GetAllOrders());
        }

        [HttpDelete(Name = "DeleteOrder")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult DeleteOrder(int id)
        {
            if (_orderRepository.GetOrder(id) == null)
            {
                return NotFound($"Order with id {id} not found.");
            }
            _orderRepository.DeleteOrder(id);
            return Ok();
        }
    }
}

