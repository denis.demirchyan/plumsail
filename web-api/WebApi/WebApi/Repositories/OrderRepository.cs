﻿using WebApi.Models;

namespace WebApi.Repositories
{
    public class OrderRepository : IOrderRepository, IDisposable
    {
        private readonly OrdersContext _context;

        public OrderRepository(OrdersContext context)
        {
            _context = context;
        }

        public void AddOrder(Order order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
        }

        public List<Order> GetAllOrders()
        {
            return _context.Orders.ToList();
        }

        public void DeleteOrder(int id)
        {
            var order = _context.Orders.Find(id);
            if (order != null)
            {
                _context.Orders.Remove(order);
                _context.SaveChanges();
            }
        }

        public Order GetOrder(int id)
        {
            return _context.Orders.Find(id);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
