export class RequestResult {
    IsSuccess = true;
    Errors = [];
    StatusCode = 0;
    Data = null;
}
