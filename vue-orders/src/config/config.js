import Home from 'pages/Home'
import Orders from 'pages/Orders'
import CreateOrder from 'pages/CreateOrder'

export const routes = [
  {
    path: '/',
    component: Home,
    name: 'Home'
  },
  {
    path: '/orders',
    component: Orders,
    name: 'Orders'
  },
  {
    path: '/orders/create',
    component: CreateOrder,
    name: 'Create order'
  }
]

export const ordersUrl = 'https://localhost:7236/Orders/'
