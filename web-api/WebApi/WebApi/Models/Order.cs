﻿using WebApi.Enums;

namespace WebApi.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public DateTime Date { get; set; }
        public Products Products { get; set; }
        public bool NeedCall { get; set; }
    }
}
